import React from 'react';
import Calculator from "./containers/Calculator/Calculator";
import {Switch, Route} from "react-router-dom";

const App = () => {
  return (
        <Switch>
            <Route path="/" exact component={Calculator}/>
            <Route render={() => <h1>Not Found</h1>}/>
        </Switch>
  );
};

export default App;
